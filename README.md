# UUID

Generates UUIDs using PECL::uuid [1] which in turn uses Linux's libuuid [2] package.

1. https://pecl.php.net/package/uuid
2. http://linux.die.net/man/3/libuuid

## Install

### MacPorts

`sudo port install php55-uuid`

### Debian

`sudo pecl install uuid`


## Usuage

see `tests/manual.php`
