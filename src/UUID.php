<?php

namespace Jbendotnet\UUID;

use Jbendotnet\UUID\Exception\InvalidTypeException;
use Jbendotnet\UUID\Exception\IsInvalidException;
use Jbendotnet\UUID\Exception\IsNullException;
/*
Generate a new uuid
string uuid_create ( [ int $uuid_type ] )
valid parameters: UUID_TYPE_DEFAULT, UUID_TYPE_TIME, UUID_TYPE_RANDOM

Check if a given uuid string is valid
bool uuid_is_valid ( string $uuid )

Compare two uuid strings
int uuid_compare ( string $uuid1, string $uuid2 )

Check if a uuid string is the null 00000000-0000-0000-0000-000000000000
bool uuid_is_null ( string $uuid )

Get the uuid’s type
int uuid_type ( string $uuid )

Get the uuid’s variant
int uuid_variant ( string $uuid )

Extract the creation time from a time based uuid as unix timestamp
int uuid_time ( string $uuid )

Get the uuid creator mac address
string uuid_mac ( string $uuid )

Convert uuid string to binary
string uuid_parse ( string $uuid )

Convert uuid binary to string
string uuid_unparse ( string $uuid )
 */

/**
 * Class UUID
 */
class UUID
{

    /**
     *
     */
    const TYPE_DEFAULT = UUID_TYPE_DEFAULT;

    /**
     *
     */
    const TYPE_TIME = UUID_TYPE_TIME;

    /**
     *
     */
    const TYPE_RANDOM = UUID_TYPE_RANDOM;

    /**
     * @var array
     */
    private $types = [
        UUID::TYPE_DEFAULT  => 'default',
        UUID::TYPE_TIME     => 'time',
        UUID::TYPE_RANDOM   => 'random'
    ];

    /**
     * @var
     */
    private $type;

    /**
     * @var
     */
    private $uuid;


    /**
     * @param $type
     * @return string
     * @throws UUIDInvalidTypeException
     */
    public function __construct($type = UUID::TYPE_DEFAULT)
    {

        $this->setType($type);

        $uuid = uuid_create($this->getType());

        $this->validate($uuid);

        $this->setUuid($uuid);

    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        if (!isset($this->types[$type])) {
            throw new InvalidTypeException('$type argument must be one of: ' . join(', ', $this->types) . $type . ' supplied');
        }
        $this->type = (string)$type;
    }

    /**
     * @param string $uuid
     */
    public function validate($uuid)
    {

        if (empty($uuid)) {
            throw new IsInvalidException('Empty $uuid');
        }

        if (!is_string($uuid)) {
            throw new IsInvalidException('$uuid must be a string, ' . gettype($uuid) . ' supplied');
        }

        if (uuid_is_null($uuid)) {
            throw new IsNullException('Null $uuid passed');
        }

        /* uuid_type IS NOT IMPLEMENTED IN PECL::uuid
        $type = uuid_type($uuid);
        if (!isset($this->types[$type])) {
            throw new UUIDInvalidTypeException('$uuid is of unexpected type: ' . $type);
        }
        */

        return true;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = (string)$uuid;
    }

    /**
     * @return string
     */
    public function __toString() {
        return (string) $this->getUuid();
    }


}