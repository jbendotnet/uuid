<?php

use Jbendotnet\UUID\UUID;
use Jbendotnet\UUID\Exception\InvalidTypeException;
use Jbendotnet\UUID\Exception\IsInvalidException;
use Jbendotnet\UUID\Exception\IsNullException;

require dirname(__DIR__) . '/vendor/autoload.php';

$uuids = [];

try {

    // gen some UUIDs
    echo 'uuid (default): ' . new UUID(UUID::TYPE_DEFAULT) . PHP_EOL;

    echo 'uuid (time): ' . new UUID(UUID::TYPE_TIME) . PHP_EOL;

    echo 'uuid (random): ' . new UUID(UUID::TYPE_RANDOM) . PHP_EOL;

    // a bad UUID
    echo 'uuid (bad): ' . new UUID('test') . PHP_EOL;

} catch (InvalidTypeException $e) {
    die($e->getMessage());
} catch (IsNullException $e) {
    die($e->getMessage());
} catch (IsInvalidException $e) {
    die($e->getMessage());
} finally {

}